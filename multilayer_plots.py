import numpy as np
import os
import sys
import xarray
import pickle
import matplotlib.pyplot as plt
import copy
import openpyxl

""" Runs the xicsrt raytracer, looping through various rotation angles on the specified axes."""

# This script requires both the xicsrt and the xicsrt_contrib packages
# to be installed or available.

# Setup the path to the xicsrt package.
# This is only needed if xicsrt is not installed as a standard package.
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt_contrib')

## Start Logging
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('PIL').setLevel(logging.WARNING)
logging.getLogger('numexpr').setLevel(logging.WARNING)


import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_raytrace
from xicsrt import xicsrt_multiprocessing
from xicsrt import xicsrt_io
from xicsrt.util import profiler
import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
import Optic_scatter_plot as Scatter

# Create output path for all saved images and results
out_path_start = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'
plot_path = os.path.join(out_path_start, 'plots')
# Setup the path to the ITER XRCS-Core scripts
# iter_script_path = 'C:/Users/color/Documents/SULI 2021/xicsrt_iter/xrcscore_npablant/scripts/'
# sys.path.append(iter_script_path)
#
# import xicsrt_iter_xrcscore_2021_08
#
# # Reload the iter script if changes have been made since the inital import.
# import importlib
# importlib.reload(xicsrt_iter_xrcscore_2021_08)

in_path = 'C:/Users/color/Documents/SULI 2021/HPCC_results/'
start_path = os.path.join(in_path, 'run05')

chords = ['magenta_1', 'red_1', 'blue_1', 'yellow_1']

ml_outputs = {}
ml_output = {}
ml_wavelengths = {}
fig, axes = plt.subplots(nrows=1, ncols=2, dpi=150, figsize=[8,4])
for chord in chords:
    ml_outputs[chord] = []
    multi_layer_path = os.path.join(start_path, chord)
    for i in range(4):
        ml_file = os.path.join(multi_layer_path, 'xrcscore_results_{}.hdf5'.format(i))
        ml_out = xicsrt_io.load_results(filename=ml_file)
        ml_outputs[chord] = ml_outputs[chord] + [ml_out]
    ml_output[chord] = xicsrt_raytrace.combine_raytrace(ml_outputs[chord])

    ml_wavelengths[chord] = ml_output[chord]['found']['history']['detector']['wavelength']
    a, bins = np.histogram(ml_wavelengths[chord], bins=15)
    if 'magenta' in chord or 'red' in chord:
        axes_ind = 1
    else:
        axes_ind = 0
    axes[axes_ind].step(bins[:-1], a, where='post', color=chord[0], label=chord)
for ax in axes:
    ax.legend()
    ax.set_xlabel('Wavelength (Angstrom)')
    ax.set_ylabel('Counts')
fig.suptitle('Detected Rays Wavelengths (15-layer HOPG)')
fig.tight_layout()
fig.savefig(os.path.join(plot_path,'Wavelength_dist_multilayer_HOPG.png'))



# ml_max = np.max(np.histogram(ml_wavelengths, bins=15)[0])

# axes[0].hist(sl_wavelengths, bins=15)
# axes[0].plot(np.median(sl_wavelengths) * np.ones(2), [0, sl_max], 'r--', label='Median')
# axes[0].set_title('Single Layer HOPG')
#
# axes[1].hist(ml_wavelengths, bins=15)
# axes[1].plot(np.median(ml_wavelengths) * np.ones(2), [0, ml_max], 'r--', label='Median')
# axes[1].set_title('15 Layer HOPG')
# for ax in axes:
#     ax.set_xlabel('Wavelength (Angstrom)')
#     ax.set_ylabel('Counts')
#     ax.legend()
# fig.suptitle('Uniform Wavelength Dist: {}'.format(chord))
# fig.tight_layout()
# fig.show()
# fig.savefig(os.path.join(plot_path, 'Single_multi_HOPG_bandwidth_{}'.format(chord)))
#
# FWHM = {'single layer':np.percentile(sl_wavelengths, 100 - (100-76.1)/2) - np.percentile(sl_wavelengths, (100-76.1)/2),
#         'multi layer':np.percentile(ml_wavelengths, 100 - (100-76.1)/2) - np.percentile(ml_wavelengths, (100-76.1)/2) }
# print('Single Layer Stats: \n'
#       + 'mean={} \n'.format(np.mean(sl_wavelengths))
#       + 'median={} \n'.format(np.median(sl_wavelengths))
#       + 'FWHM={} \n'.format(FWHM['single layer']))
#
# print('Multi-layer Stats: \n'
#       + 'mean={} \n'.format(np.mean(ml_wavelengths))
#       + 'median={} \n'.format(np.median(ml_wavelengths))
#       + 'FWHM={} \n'.format(FWHM['multi layer']))

