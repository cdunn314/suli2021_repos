from matplotlib import pyplot as plt
import numpy as np
import os
import sys

sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_io

start_path = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'
chords = ['red_1', 'blue_1']
temps = ['cold', 'hot']
fig, axes = plt.subplots(nrows=1, ncols=len(chords), dpi=150, figsize=[8, 4])
for i, chord in enumerate(chords):
    cold_file = os.path.join(start_path, 'xrcscore_results_{}_cold.hdf5'.format(chord))
    cold_data = xicsrt_io.load_results(filename=cold_file)

    hot_file = os.path.join(start_path, 'xrcscore_results_{}_hot.hdf5'.format(chord))
    hot_data = xicsrt_io.load_results(filename=hot_file)

    cold_wavelengths = cold_data['found']['history']['detector']['wavelength']
    hot_wavelengths = hot_data['found']['history']['detector']['wavelength']

    a_cold, bins_cold = np.histogram(cold_wavelengths, bins=30)
    a_hot, bins_hot = np.histogram(hot_wavelengths, bins=30)

    axes[i].step(bins_cold[:-1], a_cold, where='post', label='Cold')
    axes[i].step(bins_hot[:-1], a_hot, where='post', label='Hot')
    axes[i].legend()
    axes[i].set_xlabel('Wavelength (Angstrom)')
    axes[i].set_ylabel('Counts')
    axes[i].set_title(chord.title())

    # axes.step(bins_cold[:-1], a_cold, where='post', label='Cold')
    # axes.step(bins_hot[:-1], a_hot, where='post', label='Hot')
    # axes.legend()
    # axes.set_xlabel('Wavelength (Angstrom)')
    # axes.set_ylabel('Counts')
    # axes.set_title(chord.title())


    FWHM = {'cold': np.percentile(cold_wavelengths, 100 - (100 - 76.1) / 2)
                    - np.percentile(cold_wavelengths,(100 - 76.1) / 2),
            'hot': np.percentile(hot_wavelengths, 100 - (100-76.1)/2)
                           - np.percentile(hot_wavelengths, (100-76.1)/2)}
    print('Cold Stats: \n'
          + 'mean={} \n'.format(np.mean(cold_wavelengths))
          + 'median={} \n'.format(np.median(cold_wavelengths))
          + 'FWHM={} \n'.format(FWHM['cold'])
          + 'Detected Rays={} \n'.format(len(cold_wavelengths)))

    print('Hot Stats: \n'
          + 'mean={} \n'.format(np.mean(hot_wavelengths))
          + 'median={} \n'.format(np.median(hot_wavelengths))
          + 'FWHM={} \n'.format(FWHM['hot'])
          + 'Detected Rays={} \n'.format(len(hot_wavelengths)))

fig.suptitle("Detected Rays' Wavelength Cold vs. Hot")
fig.tight_layout()
plot_path = os.path.join(os.path.join(start_path, 'plots'), 'Cold_v_Hot_Wavelength_Dist.png')
fig.savefig(plot_path)




