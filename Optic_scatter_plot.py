import numpy as np
import pickle
import matplotlib.pyplot as plt
import sys

from numpy import ndarray
from scipy.optimize import curve_fit

sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_io
import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
import ITER_rotate_translate as Irt

def scatter_hist(x, y, ax, ax_histx, ax_histy):
    """ Creates a scatter plot in two dimensions with histograms of distributions for each dimension"""
    # Function taken from https://matplotlib.org/stable/gallery/lines_bars_and_markers/scatter_hist.html
    # no labels
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.plot(x, y, '.', markersize=5)

    # now determine nice limits:
    binwidth = np.min([np.diff(np.histogram(x)[1])[0], np.diff(np.histogram(y)[1])[0]])

    if np.max(x)-np.min(x) > np.max(y) - np.min(y):
        x1 = x
    else:
        x1 = y
    x1_range = np.max(x1) - np.min(x1)
    bins_x = np.arange(np.mean(x) - 0.6 * x1_range, np.mean(x) + 0.6 * x1_range, binwidth)
    bins_y = np.arange(np.mean(y) - 0.6 * x1_range, np.mean(y) + 0.6 * x1_range, binwidth)

    ax_histx.hist(x, bins=bins_x)
    ax_histy.hist(y, bins=bins_y, orientation='horizontal')
    ax.set_aspect('equal', adjustable='box')


def optic_scatter_hist(output, optic, save_plot=False, plot_path='', fig=[]):

    # Since I ran some files on my school computer and my laptop, the filepath for instantiating the object
    # got messed up, so the next four lines are to correct the path before putting it into instatantiate_optic
    if 'cdunn44' in output['config']['general']['pathlist_default'][0]:
        for j in range(len(output['config']['general']['pathlist_default'])):
            output['config']['general']['pathlist_default'][j] = \
                output['config']['general']['pathlist_default'][j].replace('cdunn44', 'color')
    optic_obj = Irt.instantiate_optic(output['config'], optic)

    local_coords = optic_obj.point_to_local(output['found']['history'][optic]['origin'])[:, :-1]
    y = local_coords[:, 1]
    y_median = np.median(y)
    y_mean = np.mean(y)
    y_range = np.max(y) - np.min(y)
    y_range_90 = np.percentile(y, 95) - np.percentile(y, 5)
    print('Y distribution: \n' + 'median: {}\n'.format(y_median) + 'mean: {}\n'.format(y_mean)
          + 'max: {}\n'.format(np.max(y)) + 'min: {}\n'.format(np.min(y)) +
          'full range: {}\n'.format(y_range) + '90% range: {}\n'.format(y_range_90))

    # Define figure dimensions
    left, width = 0.12, 0.6
    bottom, height = 0.12, 0.6
    spacing = 0.01

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom + height + spacing, width, 0.2]
    rect_histy = [left + width + spacing, bottom, 0.2, height]

    # start with a square Figure
    if not fig:
        fig = plt.figure(figsize=(8, 8))

    ax = fig.add_axes(rect_scatter)
    ax_histx = fig.add_axes(rect_histx, sharex=ax)
    ax_histy = fig.add_axes(rect_histy, sharey=ax)

    # use the previously defined function
    scatter_hist(local_coords[:, 0], local_coords[:, 1], ax, ax_histx, ax_histy)
    ax.set_xlabel('x', fontsize=12)
    ax.set_ylabel('y', fontsize=12)
    fig.suptitle('{} x-ray Distribution'.format(optic.title()), fontsize=12)
    if save_plot:
        fig.savefig(plot_path)
    fig.show()

