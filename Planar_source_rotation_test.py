import numpy as np
import os
import sys
import xarray
import pickle
import matplotlib.pyplot as plt
import openpyxl
import copy
# !{sys.executable} -m pip install pandas
# !{sys.executable} -m pip install openpyxl
# !{sys.executable} -m pip install plotly
# !{sys.executable} -m pip install matplotlib


# This script requires both the xicsrt and the xicsrt_contrib packages
# to be installed or available.

# Setup the path to the xicsrt package.
# This is only needed if xicsrt is not installed as a standard package.
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt_contrib')

## Start Logging
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('PIL').setLevel(logging.WARNING)
logging.getLogger('numexpr').setLevel(logging.WARNING)


import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_raytrace
from xicsrt import xicsrt_multiprocessing


from xicsrt.util import profiler
import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
import ITER_rotate_translate as Irt
import Optic_scatter_plot as scatter

# Create output path for all saved images and results
out_path_start = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'

if __name__ == '__main__':
    # Setup the path to the ITER XRCS-Core scripts
    project_path = 'C:/Users/color/Documents/SULI 2021/xicsrt_iter/xrcscore_npablant/scripts/'
    sys.path.append(project_path)

    import xicsrt_iter_xrcscore_2021_08

    # Reload the iter script if changes have been made since the inital import.
    import importlib
    importlib.reload(xicsrt_iter_xrcscore_2021_08)

    # Parameters
    load_from_file = False
    rot_ax = 'x-axis'
    deg = 0.5

    config = {}
    config['scenario'] = {}
    config['scenario']['chord'] = 'red_1'

    config = xicsrt_iter_xrcscore_2021_08.get_config(config)

    # imas_path = os.path.join(project_path, 'imas')

    config['general']['number_of_iter'] = 100
    config['general']['number_of_runs'] = 3
    config['general']['strict_config_check'] = False

    config['general']['save_results'] = True
    config['general']['save_images'] = False


    config['sources'] = {}
    config['sources']['source'] = {}

    config['sources']['source']['class_name'] = 'XicsrtSourceFocused'
    config['sources']['source']['xsize'] = 0.08
    config['sources']['source']['ysize'] = 0.11
    config['sources']['source']['spread'] = np.radians(2.1)
    config['sources']['source']['target'] = config['optics']['reflector']['origin']
    config['sources']['source']['intensity'] = 5e6
    config['sources']['source']['temperature'] = 1e4
    config['sources']['source']['mass_number'] = 131.293
    # config['sources']['source']['spread_dist'] = 'flat'

    config['sources']['source']['zsize'] = 0.0
    # config['sources']['source']['zaxis'] = [0, 1, 0]
    # config['sources']['source']['zaxis'] = (-1)*config['optics']['reflector']['zaxis']
    config['sources']['source']['zaxis'] = (-1)*config['filters']['sightline']['zaxis']
    config['sources']['source']['origin'] = config['optics']['reflector']['origin'] + 2*(config['filters']['sightline']['zaxis'])
    # config['sources']['source']['origin'] = config['filters']['sightline']['origin']
    # config['sources']['plasma']['filter_list'] = list()

    # Assign proper wavelength to chord
    if 'yellow' in config['scenario']['chord'] or 'blue' in config['scenario']['chord']:
        config['sources']['source']['wavelength'] = 2.1899
    else:
        config['sources']['source']['wavelength'] = 2.7203

    # config['sources']['source']['wavelength_dist'] = 'uniform'
    # config['sources']['source']['wavelength_range'] = (2.15, 2.75)

    config['filters']['sightline']['radius'] = 0.1

    if 'reflector' in config['optics']:
        config['optics']['reflector']['rocking_fwhm'] = 8000e-6
        config['optics']['reflector']['mosaic_depth'] = 1
        config['optics']['reflector']['check_bragg'] = True

    config['optics']['crystal']['check_bragg'] = True

    out_folder = 'rotation_scan_{:.1e}_{}_rfwhm={:.0e}'.format(
        config['sources']['source']['intensity']*config['general']['number_of_iter']*config['general']['number_of_runs'],
        config['scenario']['chord'], config['optics']['reflector']['rocking_fwhm'])

    # out_path = out_path_start + out_folder
    out_path = out_path_start

    config['general']['output_path'] = out_path
    config['general']['output_prefix'] = 'xrcscore'
    # config['general']['output_suffix'] = 'axis={}_deg={:.2f}'.format(rot_ax, deg)
    config['general']['output_suffix'] = '{}_cold'.format(config['scenario']['chord'])

    # Change size of crystal
    newConfig = copy.deepcopy(config)
    newConfig['optics']['crystal']['check_aperture'] = False
    newConfig['optics']['crystal']['check_size'] = True
    newConfig['optics']['crystal']['xsize'] = 0.1
    newConfig['optics']['crystal']['ysize'] = 0.1

    print(config['optics']['reflector']['zaxis'])


    if load_from_file:
        try:
            output = xicsrt.xicsrt_io.load_results(config=config)
            print('Loaded from file')
        except:
            load_from_file = False
            # no_data = True
            print('Could not load from file')
    ## The __name__ argument in the next line is necessary for the multiprocessing raytracer. If this if
    ## statement is left out, it will cause the raytracer to run an infinite loop through recursive functions.
    if not load_from_file:
        output_therm = xicsrt_multiprocessing.raytrace(config, processes=3)
        # newConfig = Irt.rotate(copy.deepcopy(config), 'reflector', deg, rot_ax[0])
        # newConfig = Irt.rotate(newConfig, 'crystal', -0.5, 'x', use_universal=True)
        print(newConfig['optics']['reflector']['zaxis'])
        # output = xicsrt_multiprocessing.raytrace(newConfig, processes=3)
        # output = xicsrt_raytrace.raytrace(newConfig)

    # scatter.optic_scatter_hist(output, 'crystal', fig=plt.figure(figsize=(8,8)))

    plt_refl = xicsrt_2d.plot_intersect(output_therm, 'reflector')
    plt_refl.show()
    plt_source = xicsrt_2d.plot_intersect(output_therm, 'source', 'sources')
    plt_source.show()

    # xaxis = output['config']['optics']['reflector']['xaxis']
    # zaxis = output['config']['optics']['reflector']['zaxis']
    # print([xaxis, zaxis, np.dot(xaxis, zaxis)])

    # import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
    # from xicsrt.util import mircolor
    #
    # name = 'crystal'
    # hist_bins = None
    #
    # found_color = None
    # if True:
    #     wave = output['found']['history'][name]['wavelength']
    #     # norm = mircolor.Normalize(np.percentile(wave, 5), np.percentile(wave, 95))
    #     norm = mircolor.Normalize(2.70, 2.74)
    #     grad = mircolor.getColorGradient(norm, 'coolwarm')
    #     found_color = grad.to_rgba(wave)
    #
    #
    # plt_cry_rot = xicsrt_2d.plot_intersect(
    #     output,
    #     name,
    #     hist_bins=hist_bins,
    #     found_color=found_color,
    #     lost=False,
    #     scale=1e3,
    #     units='mm'
    # )
    # plt_cry_rot.show()
    #
    # name = 'crystal'
    # hist_bins = None
    #
    # found_color = None
    # if True:
    #     wave = output2['found']['history'][name]['wavelength']
    #     # norm = mircolor.Normalize(np.percentile(wave, 5), np.percentile(wave, 95))
    #     norm = mircolor.Normalize(2.70, 2.74)
    #     grad = mircolor.getColorGradient(norm, 'coolwarm')
    #     found_color = grad.to_rgba(wave)
    #
    # plt_cry_nom = xicsrt_2d.plot_intersect(
    #     output2,
    #     name,
    #     hist_bins=hist_bins,
    #     found_color=found_color,
    #     lost=False,
    #     scale=1e3,
    #     units='mm'
    # )
    # plt_cry_nom.show()
    #
    # name = 'detector'
    # hist_bins = None
    #
    # found_color = None
    # if True:
    #     wave = output['found']['history'][name]['wavelength']
    #     # norm = mircolor.Normalize(np.percentile(wave, 5), np.percentile(wave, 95))
    #     norm = mircolor.Normalize(2.70, 2.74)
    #     grad = mircolor.getColorGradient(norm, 'coolwarm')
    #     found_color = grad.to_rgba(wave)
    #
    # plt_det_refl = xicsrt_2d.plot_intersect(
    #     output,
    #     name,
    #     hist_bins=hist_bins,
    #     found_color=found_color,
    #     lost=False,
    #     scale=1e3,
    #     units='mm'
    # )
    # plt_det_refl.show()
    #
    # name = 'detector'
    # hist_bins = None
    #
    # found_color = None
    # if True:
    #     wave = output2['found']['history'][name]['wavelength']
    #     # norm = mircolor.Normalize(np.percentile(wave, 5), np.percentile(wave, 95))
    #     norm = mircolor.Normalize(2.70, 2.74)
    #     grad = mircolor.getColorGradient(norm, 'coolwarm')
    #     found_color = grad.to_rgba(wave)
    #
    # plt_det_nom = xicsrt_2d.plot_intersect(
    #     output2,
    #     name,
    #     hist_bins=hist_bins,
    #     found_color=found_color,
    #     lost=False,
    #     scale=1e3,
    #     units='mm'
    # )
    # plt_det_nom.show()
