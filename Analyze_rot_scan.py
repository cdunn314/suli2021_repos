import numpy as np
import pickle
import matplotlib.pyplot as plt
import sys
import math
import os

from numpy import ndarray
from scipy.optimize import curve_fit

sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_io
import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
import ITER_rotate_translate as Irt

### Determine which plots to

do_rfwhm = False
do_optic_dist = False
do_bandwidth = True
do_bandwidth_rfwhm = False

# Define parameters
tot_rays = 1.5e9
chord = 'magenta_1'
chords = ['magenta_1', 'red_1']
rot_ax = 'x-axis'
optic = 'detector'
rfwhm = 8e-3
distribution = 'uniform'

# Create output path for all saved images and results
out_path_start = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'


def scatter_hist(x, y, ax, ax_histx, ax_histy, color='tab:blue'):
    """ Creates a scatter plot in two dimensions with histograms of distributions for each dimension"""
    # Function taken from https://matplotlib.org/stable/gallery/lines_bars_and_markers/scatter_hist.html
    # no labels
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.plot(x, y, '.', markersize=5, alpha=0.5, color=color)

    # now determine nice limits:
    binwidth = np.min([np.diff(np.histogram(x)[1])[0], np.diff(np.histogram(y)[1])[0]])

    if np.max(x)-np.min(x) > np.max(y) - np.min(y):
        x1 = x
    else:
        x1 = y
    x1_range = np.max(x1) - np.min(x1)
    bins_x = np.arange(np.mean(x) - 0.6 * x1_range, np.mean(x) + 0.6 * x1_range, binwidth)
    bins_y = np.arange(np.mean(y) - 0.6 * x1_range, np.mean(y) + 0.6 * x1_range, binwidth)

    ax_histx.hist(x, bins=bins_x, alpha=1.0, color=color)
    ax_histy.hist(y, bins=bins_y, orientation='horizontal', alpha=1.0, color=color)
    ax.set_aspect('equal', adjustable='box')


if do_rfwhm:
    """Create a plot of throughput vs. reflector rotation angle for several different rocking curve FWHMs"""
    # rocking curve FWHMs
    rocking_fwhms = [2000e-6, 4000e-6, 8000e-6]
    # FWHMs of fitted data from xicsrt simulations
    fitted_fwhms = np.zeros(len(rocking_fwhms))
    # define colors and symbols for plots
    colors = ['tab:blue', 'tab:green', 'tab:red']
    fmt_data = ['o', 'v', '^']

    fig, ax = plt.subplots(nrows=1, ncols=1, dpi=100, figsize=[8,5])
    for i, rfwhm in enumerate(rocking_fwhms):
        # read data from pickle file created from Planar_source_rot_scan.py
        out_folder = 'rotation_scan_{}_{:.1e}_{}_rfwhm={:.0e}'.format(distribution, tot_rays, chord, rfwhm)
        out_path = out_path_start + out_folder
        with open(out_path + '/' + out_folder + '_{}.pkl'.format(rot_ax), 'rb') as data_file:
            data = pickle.load(data_file)
        ax.errorbar(data['rot_degrees'], data['det_rays'], data['det_rays_error'], fmt=fmt_data[i], color=colors[i],
                    label='Data: rfwhm = {:.3f}$^\circ$'.format(np.degrees(rfwhm)), markersize=5, capsize=5)

        # Fit Gaussian to each curve
        x = data['rot_degrees']
        y = data['det_rays']
        n = len(x)
        mean = sum(x * y) / sum(y)
        sigma = np.sqrt(sum(y * (x - mean) ** 2) / sum(y))


        def Gauss(x, a, x0, sigma):
            return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))


        popt, pcov = curve_fit(Gauss, x, y, p0=[max(y), mean, sigma])
        # Convert standard deviation of fit to full width half max in degrees
        fitted_fwhms[i] = 2 * np.sqrt(2 * np.log(2)) * popt[2]

        ax.plot(x, Gauss(x, *popt), '--', color=colors[i], label='Fit: fwhm = {:.3f}$^\circ$'.format(fitted_fwhms[i]))
    ax.legend()
    ax.set_xlabel('Degrees of Rotation about {}'.format(rot_ax))
    ax.set_ylabel('Detected Rays')
    ax.set_title('Reflector Rotation about {} Tot_rays={:.1e} {}'.format(rot_ax, tot_rays, chord.title()))
    plt.show()
    fig.savefig(out_path_start + '/plots/' + 'refl_rot_rfwhm_scan_{}_{}_{:.1e}.png'.format(rot_ax, chord, tot_rays))

if do_optic_dist:
    """ Plot scatter plot with histograms of each direction of optic defined by 'optic' variable.
    Also creates a plot of mean x and y coordinate of intersections of rays with optic as the reflector is rotated."""
    out_folder = 'rotation_scan_{}_{:.1e}_{}_rfwhm={:.0e}'.format(distribution, tot_rays, chord, rfwhm)
    out_path = out_path_start + out_folder + '/'
    sys.path.append(out_path)
    with open(out_path + out_folder + '_{}.pkl'.format(rot_ax), 'rb') as data_file:
        throughput_data = pickle.load(data_file)
        rot_degrees = throughput_data['rot_degrees']
    # rot_degrees = [0, -0.5, 0.5]

    # 'means' variable contains mean x and y coordinate of intersections of detected rays and optic
    means = np.zeros((len(rot_degrees), 3))
    means[:, 0] = rot_degrees
    dist_legend = []
    colors = ['tab:blue', 'tab:green', 'tab:red']
    for i, deg in enumerate(rot_degrees):
        filename = 'xicsrt_results_axis={}_deg={:.2f}.hdf5'.format(rot_ax, deg)
        output = xicsrt_io.load_results(filename=out_path + filename)

        # Since I ran some files on my school computer and my laptop, the filepath for instantiating the object
        # got messed up, so the next four lines are to correct the path before putting it into instatantiate_optic
        if 'cdunn44' in output['config']['general']['pathlist_default'][0]:
            for j in range(len(output['config']['general']['pathlist_default'])):
                output['config']['general']['pathlist_default'][j] = \
                    output['config']['general']['pathlist_default'][j].replace('cdunn44', 'color')
        optic_obj = Irt.instantiate_optic(output['config'], optic)

        local_coords = optic_obj.point_to_local(output['found']['history'][optic]['origin'])[:, :-1]
        if np.round(deg, 5) == 0:
            # For 0 degree rotation (nominal case) a scatter plot with dimensional histograms is created.

            # Define figure dimensions
            left, width = 0.12, 0.6
            bottom, height = 0.12, 0.6
            spacing = 0.01

            rect_scatter = [left, bottom, width, height]
            rect_histx = [left, bottom + height + spacing, width, 0.2]
            rect_histy = [left + width + spacing, bottom, 0.2, height]

            # start with a square Figure
            # if i == 0:
            fig = plt.figure(figsize=(8, 8))

            ax = fig.add_axes(rect_scatter)
            ax_histx = fig.add_axes(rect_histx, sharex=ax)
            ax_histy = fig.add_axes(rect_histy, sharey=ax)

            # use the previously defined function
            scatter_hist(local_coords[:, 0], local_coords[:, 1], ax, ax_histx, ax_histy)
            ax.set_xlabel('x', fontsize=12)
            ax.set_ylabel('y', fontsize=12)
            dist_legend = dist_legend + ['{:.2f} $^\circ$'.format(deg)]
            fig.suptitle('Reflector {} Rotation rfwhm={:.1e} {}: {} Distribution'.format(rot_ax, rfwhm, chord,
                                                                                         optic.title()), fontsize=12)
            fig.savefig(out_path_start + '/plots/' + 'refl_rot_{}_{}_{}_dist_{}_{:.1e}.png'.format(distribution, rot_ax, optic[:3],
                                                                                                chord, rfwhm))
        # mean x-coordinate of detected rays' intersections with optic
        means[i, 1] = np.mean(local_coords[:, 0])
        # mean y-coordinate of detected rays' intersections with optic
        means[i, 2] = np.mean(local_coords[:, 1])

    # Create plot of mean x and y coordinates of intersected rays as reflector rotation angle is changed
    # Create mean y coordinate plot
    fig2, axes2 = plt.subplots(nrows=1, ncols=2, dpi=100)
    axes2[1].plot(means[:, 0], means[:, 2], '.', label='XICSRT Data', markersize=6)
    x_vals = np.linspace(min(means[:, 0]), max(means[:, 0]))
    coeff_y = np.polyfit(means[:, 0][~np.isnan(means[:, 2])], means[:, 2][~np.isnan(means[:, 2])], 1)
    axes2[1].plot(x_vals, np.polyval(coeff_y, x_vals), '--r', label='Linear Model')
    axes2[1].set_xlabel('Reflector Rotation (degrees)')
    axes2[1].set_ylabel('Mean y')
    y_axis_delta = np.diff(axes2[1].get_ybound())/2
    axes2[1].text(np.nanmean(x_vals), np.nanmean(means[:, 2]) - 0.9*y_axis_delta,
                  'y = {:.4f}x + ({:.4f})'.format(coeff_y[0], coeff_y[1]),
                  ha='center', va='center', color='red')
    axes2[1].legend(loc='upper right')

    # Create mean x coordinate plot
    axes2[0].plot(means[:, 0], means[:, 1], '.', label='XICSRT Data', markersize=6)
    coeff_x = np.polyfit(means[:, 0][~np.isnan(means[:, 1])], means[:, 1][~np.isnan(means[:, 1])], 1)
    axes2[0].plot(x_vals, np.polyval(coeff_x, x_vals), '--r', label='Linear Model')
    axes2[0].set_xlabel('Reflector Rotation (degrees)')
    axes2[0].set_ylabel('Mean x')
    # Make the x and y axes of the x vs degree plot have the same tick interval as the y vs degree plot
    axes2[0].axis([axes2[1].get_xbound()[0], axes2[1].get_xbound()[1],
                                              np.nanmean(means[:, 1]) - y_axis_delta,
                                              np.nanmean(means[:, 1]) + y_axis_delta])
    axes2[0].text(np.mean(x_vals), np.nanmean(means[:, 1]) - 0.9*y_axis_delta,
                  'y = {:.4f}x + ({:.4f})'.format(coeff_x[0], coeff_x[1]),
                  ha='center', va='center', color='red')
    axes2[0].legend(loc='upper right')
    fig2.suptitle('Reflector {} Rotation {} Mean x,y {} rfwhm={:.1e}'.format(rot_ax, optic.title(), distribution, rfwhm))
    fig2.tight_layout()

    fig2.show()
    fig2.savefig(out_path_start + '/plots/' + 'refl_rot_{}_{}_xy_{}_{}_{:.1e}.png'.format(rot_ax, optic[:3],
                                                                                          distribution, chord, rfwhm))
    ax.legend(dist_legend)
    fig.show()


def bandwidth_plots(wavelength_data, out_path, bandwidths, w_minmax, y_max,
                    rot_degrees, rot_ax, rfwhm, chord, tot_rays, fig2=[], ax2=[]):
    w_range = w_minmax[1] - w_minmax[0]
    bins = np.arange(w_minmax[0] - w_range * 0.1, w_minmax[1] + w_range * 0.1, w_range/20)
    for i, deg in enumerate(rot_degrees):
        fig, ax = plt.subplots(nrows=1, ncols=1, dpi=100)
        # bins = np.histogram(wavelength_data[str(deg)])[1]
        ax.hist(wavelength_data[str(deg)], bins=bins)
        ax.plot(wavelength_data['median'][i] * np.ones(2), [0, y_max], 'r--', label='Median')
        ax.set_xlabel('Wavelength (Angstrom)')
        ax.set_title('Refl {:.2f}$^\circ$ {} Rotation {} Bandpass: {}'.format(deg, rot_ax,
                                                                                   optic.title()[:3], chord), fontsize=14)

        ax.set_xlim([w_minmax[0] - w_range * 0.1, w_minmax[1] + w_range * 0.1])
        ax.set_ylim([0, 1.1 * y_max])
        ax.legend()
        fig.savefig(out_path + '/bandwidth/' +
                    'refl_rot_uniform_{:.2f}_{}_{}_band_{}_{:.1e}.png'.format(deg, rot_ax, optic[:3], chord, rfwhm))
        # fig.show()
        plt.close()
    if not fig2:
        fig2, ax2 = plt.subplots(nrows=1, ncols=3, dpi=100, figsize=[10,4])
    for i,bw_name in enumerate(bandwidths.keys()):

        ax2[i].plot(bandwidths[bw_name][:, 0], bandwidths[bw_name][:, 1], '.-', label=chord, color=chord[0])
        ax2[i].set_xlabel('Reflector Rotation (degrees)')
        ax2[i].set_ylabel('Bandwidth (Angstrom)')
        ax2[i].set_title(bw_name + '%')
        ax2[i].legend()
    fig2.suptitle('Reflector Rotation ({}) Bandwidth rfwhm={:.0e}'.format(rot_ax, rfwhm))
    fig2.tight_layout()
    fig2.savefig('C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/plots/'
                 + 'Refl_rot_{}_uniform_bandwidth_{:.1e}_{:.0e}.png'.format(rot_ax, tot_rays, rfwhm))
    fig2.show()
    return


def endpoints_plot(endpoints_data, plot_dir, rot_ax, rfwhm, chord, tot_rays):
    fig, axes = plt.subplots(nrows=1, ncols=2, dpi=150, figsize=(7, 4))
    for bw_name in endpoints_data.keys():
        axes[0].plot(endpoints_data[bw_name][:, 0], endpoints_data[bw_name][:, 1], '.-', label=bw_name)
        axes[0].set_title('Distribution Lower Endpoint')
        axes[1].plot(endpoints_data[bw_name][:, 0], endpoints_data[bw_name][:, 2], '.-', label=bw_name)
        axes[1].set_title('Distribution Upper Endpoint')
        for ax in axes:
            ax.set_xlabel('Reflector Rotation Angle ($^\circ$)')
            ax.set_ylabel('Wavelength (Angstrom)')
            ax.legend()
    fig.suptitle('Reflector Rot ({}) Detector Dist Endpoints: {} rfwhm={:.0e}'.format(rot_ax, chord, rfwhm))
    fig.tight_layout()
    fig.savefig(os.path.join(plot_dir,
                             'Refl_rot_{}_det_endpoint_{:.1e}_{}_{:.0e}.png'.format(rot_ax, tot_rays, chord, rfwhm)))
    fig.show()

    return

def average_plot(wavelength_data, rot_degrees, plot_dir, rot_ax, rfwhm, chord, tot_rays):
    fig, ax = plt.subplots(nrows=1, ncols=1, dpi=150)
    ax.plot(rot_degrees, wavelength_data['mean'], '.-', label='Mean')
    ax.plot(rot_degrees, wavelength_data['median'], '*-', label='Median')
    ax.legend()
    ax.set_xlabel('Reflection Rotation Angle ($^\circ$)')
    ax.set_ylabel('Wavelength (Angstrom)')
    ax.set_title('Refl Rot ({}) Detector Average Wavelength: {} rfwhm={:.0e}'.format(rot_ax, chord, rfwhm))
    fig.savefig(os.path.join(plot_dir,
                             'Refl_rot_{}_det_average_{:.1e}_{}_{:.0e}.png'.format(rot_ax, tot_rays, chord, rfwhm)))
    fig.show()

    return fig,ax


def do_bandwidth_stats(out_path_start, optic, rfwhm, tot_rays, chords, do_bandwidth_hist=False,
                       do_endpoints_plot=True, do_ave_plot=True):
    if do_bandwidth_hist:
        fig, axes = plt.subplots(nrows=1, ncols=2, figsize=[7, 4])
    for chord in chords:
        out_folder = 'rotation_scan_uniform_{:.1e}_{}_rfwhm={:.0e}'.format(tot_rays, chord, rfwhm)
        out_path = out_path_start + out_folder + '/'
        sys.path.append(out_path)

        # Make bandwidth folder inside scan results folder if it hasn't already been made.
        bandwidth_path = os.path.join(out_path, 'bandwidth')
        if not os.path.isdir(bandwidth_path):
            os.mkdir(bandwidth_path)

        with open(out_path + out_folder + '_{}.pkl'.format(rot_ax), 'rb') as data_file:
            throughput_data = pickle.load(data_file)
            rot_degrees = np.round(throughput_data['rot_degrees'],2)

        wavelength_data = {'mean': np.zeros(len(rot_degrees)),'median': np.zeros(len(rot_degrees))}
        endpoints_data = {}
        bandwidths = {}
        sigma_fwhm = 2*np.sqrt(2*np.log(2))/2
        fwhm_per = ((1 + math.erf(sigma_fwhm / np.sqrt(2))) - (1 + math.erf(0.0 / np.sqrt(2)))) * 100
        # bw_perc = {'100': [], '90': [], str(round(fwhm_per, 2)): []}
        bw_perc = {'100': [], str(round(fwhm_per, 2)): []}
        max_w = 0
        min_w = np.inf
        y_max = 0
        for bw_name in bw_perc.keys():
            endpoints_data[bw_name] = np.zeros((len(rot_degrees), 3))
            endpoints_data[bw_name][:, 0] = rot_degrees
            bandwidths[bw_name] = np.zeros((len(rot_degrees), 2))
            bandwidths[bw_name][:, 0] = rot_degrees
            bw_perc[bw_name] = {}
            lower_per = (100 - float(bw_name))/2
            upper_per = 100 - lower_per
            bw_perc[bw_name]['lower'] = lower_per
            bw_perc[bw_name]['upper'] = upper_per
        for i, deg in enumerate(rot_degrees):
            filename = 'xicsrt_results_axis={}_deg={:.2f}.hdf5'.format(rot_ax, deg)
            output = xicsrt_io.load_results(filename=out_path + filename)
            wavelengths = output['found']['history'][optic]['wavelength']
            wavelength_data[str(deg)] = wavelengths
            wavelength_data['mean'][i] = np.mean(wavelengths)
            wavelength_data['median'][i] = np.median(wavelengths)
            if wavelengths.size > 0:
                for bw_name in bw_perc.keys():
                    endpoints_data[bw_name][i, 1] = np.percentile(wavelengths, bw_perc[bw_name]['lower'])
                    endpoints_data[bw_name][i, 2] = np.percentile(wavelengths, bw_perc[bw_name]['upper'])
                    bandwidths[bw_name][i, 1] = endpoints_data[bw_name][i, 2] - endpoints_data[bw_name][i, 1]

                if max_w < np.max(wavelengths):
                    max_w = np.max(wavelengths)
                if min_w > np.min(wavelengths):
                    min_w = np.min(wavelengths)
                a = np.histogram(wavelength_data[str(deg)], bins=20)[0]
                if y_max < np.max(a):
                    y_max = np.max(a)
            else:
                for bw_name in bw_perc.keys():
                    endpoints_data[bw_name][i, 1:] = np.NaN

    # Plot and save histograms

        plot_dir = os.path.join(out_path_start, 'plots')
        if do_bandwidth_hist:

            bandwidth_plots(wavelength_data, out_path, bandwidths, [min_w, max_w],
                            y_max, rot_degrees, rot_ax, rfwhm, chord, tot_rays, fig2=fig, ax2=axes)
        if do_endpoints_plot:
            endpoints_plot(endpoints_data, plot_dir, rot_ax, rfwhm, chord, tot_rays)
        if do_ave_plot:
            average_plot(wavelength_data, rot_degrees, plot_dir, rot_ax, rfwhm, chord, tot_rays)
    return wavelength_data, bandwidths, endpoints_data


if do_bandwidth:
    do_bandwidth_stats(out_path_start, optic, rfwhm, tot_rays, chords, do_bandwidth_hist=True,
                       do_endpoints_plot=True, do_ave_plot=True)

if do_bandwidth_rfwhm:
    rfwhms = [8e-3, 4e-3, 2e-3]
    rot_degrees = [-0.5, 0.0, 0.5]
    bw_name = '90'
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(9, 6))
    fig_med, ax_med = plt.subplots(nrows=1, ncols=1, dpi=150)
    fig_end, axes_end = plt.subplots(nrows=1, ncols=2, dpi=150)
    for rfwhm in rfwhms:
        wavelength_data, bandwidths, endpoints_data = do_bandwidth_stats(out_path_start, optic, rfwhm, tot_rays,
                                                                         chord, do_bandwidth_hist=False,
                                                                         do_endpoints_plot=False, do_ave_plot=False)
        for i, ax in enumerate(axes):
            ax.hist(wavelength_data[str(rot_degrees[i])], alpha=1.0, label='rfwhm={:.0e}'.format(rfwhm))
            ax.set_xlabel('Wavelength (Angstrom)')
            ax.set_title('{:.1f}$^\circ$'.format(rot_degrees[i]))
            ax.legend(fontsize=10)
            ax.set_ylim(0, 480)
            ax.set_xlim(2.697, 2.745)
        ax_med.plot(bandwidths['90'][:, 0], wavelength_data['median'], '*-', label='rfwhm={:.0e}'.format(rfwhm))
        ax_med.legend()
        ax_med.set_xlabel('Reflection Rotation Angle ($^\circ$)')
        ax_med.set_ylabel('Wavelength (Angstrom)')


        axes_end[0].plot(endpoints_data[bw_name][:, 0], endpoints_data[bw_name][:, 1], '.-', label='rfwhm={:.0e}'.format(rfwhm))
        axes_end[0].set_title('Distribution Lower Endpoint')
        axes_end[1].plot(endpoints_data[bw_name][:, 0], endpoints_data[bw_name][:, 2], '.-', label='rfwhm={:.0e}'.format(rfwhm))
        axes_end[1].set_title('Distribution Upper Endpoint')
        for ax_end in axes_end:
            ax_end.set_xlabel('Reflector Rotation Angle ($^\circ$)')
            ax_end.set_ylabel('Wavelength (Angstrom)')
            ax_end.legend()
    fig_med.suptitle('Refl Rot ({}) Detector Median Wavelength: {}'.format(rot_ax, chord, rfwhm))
    fig_end.suptitle('Reflector Rot ({}) Detector Dist Endpoints: {}'.format(rot_ax, chord))
    fig.suptitle('Detected Ray Wavelength Distribution with {} Rotation'.format(rot_ax))
    fig.tight_layout()

    plot_savepath = os.path.join(out_path_start, 'plots')
    fig_savepath = os.path.join(plot_savepath, 'Refl_{}_rot_wavelength_dist_rfwhm_scan_{}.png'.format(rot_ax, chord))
    fig.savefig(fig_savepath)
    fig.show()

    fig_med.savefig(os.path.join(plot_savepath,
                             'Refl_rot_{}_det_median_rfwhm_scan_{:.1e}_{}.png'.format(rot_ax, tot_rays, chord)))
    fig_med.show()

    fig_end.savefig(os.path.join(plot_savepath,
                                 'Refl_rot_{}_det_endpoint_rfwhm_scan_{:.1e}_{}.png'.format(rot_ax, tot_rays, chord)))
    fig_end.show()








