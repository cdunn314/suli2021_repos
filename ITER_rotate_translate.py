import sys
from scipy.spatial.transform import Rotation as Rot

sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
from xicsrt import xicsrt_public

"""Module to rotate and translate optic objects in the ITER core model"""


def instantiate_optic(config, object_name):
    """ Instantiate optics object"""
    obj = xicsrt_public.get_element(config, object_name)
    return obj


def rotate(config, object_name, degree, axis, use_universal=False):

    """Rotates z-axis vector from config object about the specified axis.
    Options for axis are 'z', 'y', and 'x'. """
    # Instantiate object
    obj = instantiate_optic(config, object_name)

    if use_universal:
        old_z_axis = config['optics'][object_name]['zaxis']
        old_x_axis = config['optics'][object_name]['xaxis']
    else:
        old_z_axis = obj.vector_to_local(config['optics'][object_name]['zaxis'])
        old_x_axis = obj.vector_to_local(config['optics'][object_name]['xaxis'])

    rot_obj = Rot.from_euler(axis, degree, degrees=True)
    new_z_axis = rot_obj.apply(old_z_axis)
    new_x_axis = rot_obj.apply(old_x_axis)

    if use_universal:
        config['optics'][object_name]['zaxis'] = new_z_axis
        config['optics'][object_name]['xaxis'] = new_x_axis
    else:
        config['optics'][object_name]['zaxis'] = obj.vector_to_external(new_z_axis)
        config['optics'][object_name]['xaxis'] = obj.vector_to_external(new_x_axis)

    return config


def translate(config, object_name, translation_vec, use_local=False):
    """ Performs translation on optics object by adding the translation vector (translation_vec) to the origin
    found in the config. """
    old_origin = config['optics'][object_name]['origin']
    if use_local:
        obj = instantiate_optic(config, object_name)
        origin_local = obj.point_to_local(old_origin) + translation_vec
        origin = obj.point_to_external(origin_local)
    else:
        origin = old_origin + translation_vec

    config['optics'][object_name]['origin'] = origin

    return config
