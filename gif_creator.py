import numpy as np
from PIL import Image, ImageFont, ImageDraw

tot_rays = 1.5e9
chord = 'red_1'
rfwhm = 8e-3
rot_ax = 'x-axis'
optic = 'detector'
rot_degrees = np.arange(-0.3, 0.3 + 0.01, 0.025)

out_dir = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'

out_folder = 'rotation_scan_uniform_{:.1e}_{}_rfwhm={:.0e}/bandwidth/'.format(tot_rays, chord, rfwhm, rot_ax)
in_path = out_dir + out_folder
pathlist = []
for deg in rot_degrees:
    image_file = 'refl_rot_uniform_{:.2f}_{}_{}_band_{}_{:.1e}.png'.format(deg, rot_ax, optic[:3], chord, rfwhm)
    pathlist = pathlist + [in_path + image_file]
save_path = in_path + 'gif_refl_rot_{}_{}_band_{}_{:.0e}.gif'.format(rot_ax, optic[:3], chord, rfwhm)

def gif_creator(pathlist, save_path, duration=210, text_list=[], text_loc=[0,0,0],
                font=ImageFont.truetype("arial.ttf", 25), text_color='black'):
    """ Creates gif from a series of images saved to a list called 'pathlist' and saves
    it to 'save_path'.
    Optional text can be added to the image if desired, with the center location of text
    being the 'text_loc' variable. """

    imgs = ()
    for i, path in enumerate(pathlist):
        my_image = Image.open(path)
        if bool(text_list):
            image_editable = ImageDraw.Draw(my_image)
            image_editable.text(text_loc, text_list[i], fill=text_color, anchor='mm', font=font)
        imgs = imgs + (my_image,)
    my_image.save(fp=save_path, format='GIF', append_images=imgs, save_all=True, duration=duration, loop=0)
    return


gif_creator(pathlist, save_path)
