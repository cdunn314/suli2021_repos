import numpy as np
import os
import sys
import xarray
import pickle
import matplotlib.pyplot as plt
import copy
import openpyxl

# !{sys.executable} -m pip install pandas
# !{sys.executable} -m pip install openpyxl
# !{sys.executable} -m pip install plotly
# !{sys.executable} -m pip install matplotlib


# This script requires both the xicsrt and the xicsrt_contrib packages
# to be installed or available.

# Setup the path to the xicsrt package.
# This is only needed if xicsrt is not installed as a standard package.
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt')
sys.path.append('C:/Users/color/Documents/SULI 2021/xicsrt_contrib')

## Start Logging
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('PIL').setLevel(logging.WARNING)
logging.getLogger('numexpr').setLevel(logging.WARNING)


import xicsrt
import xicsrt_contrib
from xicsrt import xicsrt_raytrace
from xicsrt import xicsrt_multiprocessing
from xicsrt import xicsrt_io
from xicsrt.util import profiler
import xicsrt.visual.xicsrt_2d__matplotlib as xicsrt_2d
import ITER_rotate_translate as Irt

# Create output path for all saved images and results
out_path_start = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'

if __name__ == '__main__':
    # Setup the path to the ITER XRCS-Core scripts
    project_path = 'C:/Users/color/Documents/SULI 2021/xicsrt_iter/xrcscore_npablant/scripts/'
    sys.path.append(project_path)

    import xicsrt_iter_xrcscore_2021_03

    # Reload the iter script if changes have been made since the inital import.
    import importlib
    importlib.reload(xicsrt_iter_xrcscore_2021_03)

    # Set up variables for Translation scan
    trans_vec = np.arange(-0.04, 0.04 + 0.001, 0.005)
    det_rays = {}
    # trans_axes = ['x-axis', 'y-axis']
    trans_axes = ['x-axis', 'y-axis', 'z-axis']
    load_from_file = True
    no_data = False

    for trans_ax in trans_axes:
        det_rays[trans_ax] = np.zeros(len(trans_vec))

    # Set up configuration
    config = {}
    config['scenario'] = {}
    config['scenario']['chord'] = 'magenta_1'

    config = xicsrt_iter_xrcscore_2021_03.get_config(config)

    print(config['filters']['sightline']['origin'])
    print(config['optics']['reflector']['origin'] + config['filters']['sightline']['zaxis'])
    print(config['optics']['reflector']['origin'])

    # imas_path = os.path.join(project_path, 'imas')

    config['general']['number_of_iter'] = 50
    config['general']['number_of_runs'] = 3
    config['general']['strict_config_check'] = False
    config['general']['save_results'] = True
    config['general']['save_images'] = True


    config['sources'] = {}
    config['sources']['source'] = {}

    config['sources']['source']['class_name'] = 'XicsrtSourceFocused'
    config['sources']['source']['xsize'] = 0.08
    config['sources']['source']['ysize'] = 0.11
    config['sources']['source']['spread'] = np.radians(1.3)
    config['sources']['source']['target'] = config['optics']['reflector']['origin']
    config['sources']['source']['intensity'] = 5e6
    config['sources']['source']['temperature'] = 0
    # config['sources']['source']['spread_dist'] = 'flat'

    config['sources']['source']['zsize'] = 0
    # config['sources']['source']['zaxis'] = [0, 1, 0]
    # config['sources']['source']['zaxis'] = (-1)*config['optics']['reflector']['zaxis']
    config['sources']['source']['zaxis'] = (-1)*config['filters']['sightline']['zaxis']
    config['sources']['source']['origin'] = config['optics']['reflector']['origin'] + 2*(config['filters']['sightline']['zaxis'])
    # config['sources']['source']['origin'] = config['filters']['sightline']['origin']
    # config['sources']['plasma']['filter_list'] = list()

    # Assign proper wavelength to chord
    if 'yellow' in config['scenario']['chord'] or 'blue' in config['scenario']['chord']:
        config['sources']['source']['wavelength'] = 2.1899
    else:
        config['sources']['source']['wavelength'] = 2.7203

    config['sources']['source']['wavelength_dist'] = 'voigt'
    # config['sources']['source']['wavelength_range'] = (2.7203, 2.7203)
    distribution = config['sources']['source']['wavelength_dist']

    config['filters']['sightline']['radius'] = 0.1

    if 'reflector' in config['optics']:
        config['optics']['reflector']['rocking_fwhm'] = 8000e-6
        config['optics']['reflector']['mosaic_depth'] = 1
        config['optics']['reflector']['check_bragg'] = True

    config['optics']['crystal']['check_bragg'] = True

    out_path_start = 'C:/Users/color/Documents/SULI 2021/xrcscore_cdunn/results/'
    out_folder = 'translation_scan_{}_{:.1e}_{}_rfwhm={:.0e}'.format(distribution,
        config['sources']['source']['intensity']*config['general']['number_of_iter']*config['general']['number_of_runs'],
        config['scenario']['chord'], config['optics']['reflector']['rocking_fwhm'])
    out_path = out_path_start + out_folder

    if not os.path.isdir(out_path):
        os.mkdir(out_path)

    config['general']['output_path'] = out_path
    config['general']['output_prefix'] = 'xicsrt'

    # Execute Translation scan
    fig, ax = plt.subplots(1)
    for trans_ax in trans_axes:
        if 'x' in trans_ax[0]:
            vec = np.array([1, 0, 0])
        elif 'y' in trans_ax[0]:
            vec = np.array([0, 1, 0])
        elif 'z' in trans_ax[0]:
            vec = np.array([0, 0, 1])

        for i, inc in enumerate(trans_vec):
            no_data = False
            print('axis:{} incree:{}'.format(trans_ax, inc))
            config['general']['output_suffix'] = 'axis={}_inc={:.3f}'.format(trans_ax, inc)

            if load_from_file:
                try:
                    output = xicsrt_io.load_results(config=config)
                    print('Loaded from file')
                except:
                    load_from_file = False
                    # no_data = True
                    ValueError('Could not load from file')
            if not load_from_file:
                newConfig = Irt.translate(copy.deepcopy(config), 'reflector', inc * vec)
                output = xicsrt_raytrace.raytrace(newConfig)
                # output = xicsrt_multiprocessing.raytrace(newConfig, processes=3)

            if no_data:
                num_det_rays = np.NaN
            else:
                total_rays = output['total']['meta']['source']['num_out']
                num_det_rays = np.sum(output['found']['history']['detector']['mask'])

            det_rays[trans_ax][i] = num_det_rays
        with open(out_path + '/' + out_folder + '_{}.pkl'.format(trans_ax), 'wb') as data_file:
            data = {'trans_vec': trans_vec, 'det_rays': det_rays[trans_ax],
                    'det_rays_error':np.sqrt(det_rays[trans_ax]), 'total_rays': total_rays}
            pickle.dump(data, data_file)
        ax.errorbar(trans_vec, det_rays[trans_ax], np.sqrt(det_rays[trans_ax]),
                    fmt='.-', capsize=1.5)

    ax.legend(trans_axes)
    ax.set_xlabel('Translation on Axis (meters)')
    ax.set_ylabel('Detected Rays')
    ax.set_title('Reflector Translation Detector Throughput: {}'.format(config['scenario']['chord']))
    plt.show()
    plot_path = os.path.join(os.path.join(out_path_start, 'plots'),
                             'Refl_trans_throughput_{}_{}_{:.1e}_{:.0e}.png'.format(distribution,
                                                                                config['scenario']['chord'],
                                                                                total_rays,
                                                                                config['optics']['reflector']['rocking_fwhm']))
    fig.savefig(plot_path)
    # xicsrt_2d.plot_intersect(output, 'reflector')


